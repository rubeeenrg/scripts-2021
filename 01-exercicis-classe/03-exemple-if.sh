#! /bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Exemple if: indica si és major d'edat
# $ programa edad
# -------------------------
# 1) Validem arguments:
if [ $# -ne 1 ]
then
  echo "ERROR: nº arguments incorrecte"
  echo "Usage: $0 edad"
  exit 1
fi

# 2) Xixa
edad=$1
if [ $edad -ge 18 ]
then
  echo "Edad $edad major d'edad"
fi

exit 0

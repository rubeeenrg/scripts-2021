#! bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Descripció: Llistar dir
# programa $dir
# Validem que existeix 1 arg i que sigui un directori
# ACCIÓ: Fer un "ls" del $dir
# -----------------------------------

# CONSTANTS
ERR_NARGS=1
ERR_NODIR=2
OK=0

# 1) Validem que hi ha un argument:
if [ $# -ne 1 ]
then
   echo "ERROR: nº arguments incorrecte"
   echo "Usage: $0 dir"
   exit $ERR_NARGS
fi

# 2) Validem que sigui un directori, si no ho és...
if ! [ -d $1 ]
then
  echo "ERROR: $1 no és un directori"
  echo "Usage: $0 dir"
  exit $ERR_NODIR
fi

# 3) XIXA:
dir=$1
ls $dir
exit $OK

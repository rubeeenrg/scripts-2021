#! /bin/bash
# Rubén Rodríguez García
# Febrer 2021
#-----------------------------------------

# for $var in llista_valors
# do
#    accions
# done

# $* i $@ és diferèncien en què "$@" s'expandeix igualment

# 5) Llistar numerats tots els logins de /etc/passwd:
llista=$(cut -f1 -d: /etc/passwd)
num=1
for arg in $llista
do
  echo "$num: $arg"
  ((num++))
done

exit 0

# 5) Llistat dels noms del directori actiu numerat línea a línea:
llista=$(ls)
num=1
for arg in $llista
do
  echo "$num: $arg"
  ((num++))
done

exit 0

# 4) Llistar els arguments numerats:
num=1
for arg in $*
do
  echo "$num: $arg"
  num=$((num+1))
done

exit 0

# 3) Iterar per la llista de noms de fitxers que genera "ls":
llistat=$(ls)
for nom in $llistat
do
  echo $nom
done

exit 0

# 2b) Iterar per cada argument rebut
for arg in "$@"
do
  echo $arg
done

exit 0

# 2a) Iterar per cada argument rebut
for arg in $* 
do
  echo $arg
done

exit 0

# 1c) Iterar una llista de noms
for nom in "pere marta anna ramón"
do
  echo $nom
done

exit 0

# 1b) Iterar una llista de noms
for nom in pere marta anna ramón
do
  echo $nom
done

exit 0

# 1a) Iterar una llista de noms
for nom in "pere" "marta" "anna" "ramón"
do
  echo $nom
done

exit 0

#! /bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Exemples "case"
# -------------------------

# 3) Exemples dies "laborables / festius":
case $1 in
   "dl"|"dt"|"dc"|"dj"|"dv")
    echo "és laborable";;
   "ds"|"dm")
    echo "és festiu";;
   *)
    echo "això no és un dia de la setmana"
esac


exit 0

# 2) Exemples "vocals / consonants":
case $1 in
  [aeiou])
    echo "és una vocal";;
  [bcdfghjklmnpqrstvwxyz])
    echo "és una consonant";;
  *)
    echo "és una altra cosa"
esac

exit 0

# 1) Exemple "noms":
case $1 in
  "pere"|"pau"|"joan")
      echo "és un nen";;
  "marta"|"anna"|"julia")
      echo "és una nena"
      ;;
  *)
      echo "indefinit";;
esac

exit 0

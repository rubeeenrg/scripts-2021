#! bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Descripció: Exemples bucles "while"
# -----------------------------------

# while [condició]
# do
#   accions
# done

# 9) Procesar entrada estàndar (stdin) amb les línes numerades i en majúsucles:
num=1
while read -r line
do
  echo "$num: $line" | tr '[a-z]' '[A-Z]'
  ((num++))
done

exit 0

# 8) Procesar entrada estàndar (stdin) fins que escribim un TOKEN de sortida --> EX: FI:
read -r line
while [ "$line" != "FI" ]
do
  echo "$line"
  read -r line
done

exit 0

# 7) Procesar entrada estàndar (stdin) amb les línes numerades:
num=1
while read -r line
do
  echo "$num: $line"
  ((num++))
done

exit 0

# 6) Procesar entrada estàndar (stdin):
while read -r line 
do
  echo "$line"
done

exit 0

# 5) Mostrar els arguments:
num=1
while [ $# -gt 0 ]
do
  echo "$num: $1, $#, $*" # Ens mostra el primer argument, quants n'hi ha i mostra tots els arguments que hi han.
  num=$((num++))
  shift
done

exit 0

# 4.1) Mostrar els arguments:
num=1
while [ -n "$1" ]
do
  echo "$num: $1, $#, $*" # Ens mostra el primer argument, quants n'hi ha i mostra tots els arguments que hi han.
  num=$((num++))
  shift
done

exit 0

# 3) Mostrar els arguments:
while [ -n "$1" ]
do
  echo "$1 $#: $*"
  shift
done

exit 0

# 2) Mostrar de n a 0 countdown:
MIN=0
num=$1
while [ $num -ge $MIN ]
do
  echo -n "$num, "
  ((num--))
done

exit 0

# 1) Mostrar números del 1 al 10:
MAX=10
num=1
while [ $num -le $MAX ]
do
  echo "$num"
  ((num++))
done

exit 0

#! bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Descripció: Copiar $[-a, -b, -c, -d, -f, -g] $arg[...]
# Primer s'escriuen les opcions i després els arguments.
# programa $[-a, -b, -c, -d, -f, -g](case) $arg[...]
# SORTIDA: opcions: ......
#          arguments: ......
#
# EX: prog -b -c arg1 -d arg2 arg3
# SORTIDA: opcions: -b -c -d
#          arguments: arg1 arg2 arg3
# ------------------------------------------

# CONSTANTS:
ERR_NARGS=1
OK=0

# 1) Validem que hi hagin + de 0 args:
if [ $# -eq 0 ]
then
   echo "ERROR: nº arguments invàlid."
   echo "Usage: $0 [-a, -b, -c, -d, -f, -g] arg[..]"
   exit $ERR_NARGS
fi

# 2) XIXA
opcions=""
arguments=""

for arg in $*
do
  case $arg in
      "-a"|"-b"|"-c"|"-d"|"-f"|"-g")
	  opcions="$opcions $arg";;
      *)
	  arguments="$arguments $arg";;
  esac
done

echo "Opcions: $opcions"
echo "Arguments: $arguments"
exit $OK

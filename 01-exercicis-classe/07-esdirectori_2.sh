#! /bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Llistar el directori si existeix o l'ajuda si es posa -h.
# Validar que hi ha un argument.
# Si l'argument rebut és un directori. (Si no és directori --> ERROR)
#				       (Si no ha posat -h --> ERROR)
# Llistar-ho
# $ programa $mydir
# $ programa $-h
# -------------------------
# CONSTANTS
ERR_NARGS=1
ERR_NODIR=2

# 1) Validem que hi ha un argument:
if [ $# -ne 1 ]
then
  echo "ERROR: nº arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# 2) Validar i help
if [ $1 = "-h" ]
then
  echo "Help de la ordre 07-esdirectori_2.sh"
  echo "Rubén Rodríguez Garcia, m01, curs 2020-21"
  exit 0
fi

# 3) Validem que sigui un directori, si no ho és...
mydir=$1
if ! [ -d $mydir ]
then
  echo "ERROR: $mydir no és un directori"
  echo "Usage: $0 dir"
  exit $ERR_NODIR
fi

# 4) XIXA
ls $mydir
exit 0

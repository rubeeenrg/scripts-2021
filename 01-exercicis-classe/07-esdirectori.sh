#! /bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Llistar el directori si existeix.
# Validar que hi ha un argument.
# Si l'argument rebut és un directori. (Si no és directori --> ERROR)
# Llistar-ho
# $ programa $dir
# -------------------------
# CONSTANTS
ERR_NARGS=1
ERR_NODIR=2

# 1) Validem que hi ha un argument:
if [ $# -ne 1 ]
then
  echo "ERROR: nº arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# 2) Validem que sigui un directori, si no ho és...
mydir=$1
if ! [ -d $mydir ]
then
  echo "ERROR: $mydir no és un directori"
  echo "Usage: $0 dir"
  exit $ERR_NODIR
fi

# 3) XIXA
ls $mydir
exit 0

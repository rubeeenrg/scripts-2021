#! /bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Primer exemple script
# -------------------------
echo "Hello World"
echo "Bon dia, avui plou"
nom="Rubén Rodríguez García"
edad=18
echo $nom $edad
echo -e "nom: $nom\n edad: $edad"
echo -e 'nom: $nom\n edad: $edad'
uname -a
uptime
echo $SHELL
echo $SHLVL
echo $((4*32))
echo $(($edad*2))
#read data1 data2
#echo -e "$data1 \n $data2"
exit 0

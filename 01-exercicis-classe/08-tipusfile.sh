#! /bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Llistar si es un dir, un link o un regular file.
# Validar que hi ha un argument.
# Si l'argument rebut és un directori. (Si no és directori, regular file o symbolic link --> echo "...")
# Llistar-ho
# $programa $mylink
# $programa $myfile
# $programa $mydir
# -------------------------

ERR_NARGS=1
ERR_NOEXIST=2

# 1) Validem arguments:
if [ $# -ne 1 ]
then
  echo "ERROR: nº arguments incorrecte"
  echo "Usage: $0 dir OR $0 file OR $0 link"
  exit $ERR_NARGS
fi

# 2) XIXA:
arg=$1
if ! [ -e $arg ]; then
  echo "ERROR: $arg no existeix"
  exit $ERROR_NOEXIST
elif [ -h $arg ]; then 
  echo "$arg és un symbolic link."
elif [ -f $arg ]; then
  echo "$arg és un regular file."
elif [ -d $arg ]; then
  echo "$arg és un directori."
else
  echo "$arg és un altra cosa."
fi

exit 0

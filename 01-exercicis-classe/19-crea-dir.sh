#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Crear director-hi --> BÉ? --> stdout
#                   --> ERROR? --> stderror
# L'ordre "mkdir" no genera sortida
# Validar que hi ha 1 o més arguments.
# Si tot va bé --> 0
# Error en argument --> 1
# Algún error en crear --> 2
# programa noudir[...]
# ------------------------------------------

# CONSTANTS
ERR_NARGS=1
ERR_MKDIR=2
status=0

# 1) Validem que hi ha 1 o més arguments:
if [ $# -eq 0 ]
then
   echo "ERROR: nº arguments incorrecte"
   echo "Usage: $0 noudir[...]"
   exit $ERR_NARGS
fi

# 2) XIXA:
for dir in $*
do
  mkdir -p "$dir" &> /dev/null
  if [ $? -eq 0 ]
  then
    echo "$dir"
  else
    echo "Error en crear $dir" >> /dev/stderr
    status=$ERR_MKDIR
  fi
done

exit $status

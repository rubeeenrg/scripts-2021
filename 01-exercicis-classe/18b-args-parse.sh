#! bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Primer s'escriuen les opcions i després els arguments.
# programa [-a file -b -c -dmin max -e] arg[...]
# ------------------------------------------

# -n --> quan hi hagi contingut (no estigui buit) | "$1" (cometes), ja que és un string
# shift --> desplaçem l'argument i passem al següent

# CONSTANTS:
OK=0

# 1) XIXA :
opcions=""
args=""
file=""
min=""
max=""
while [ -n "$1" ]
do
  case $1 in
       -[bce])
          opcions="$opcions $1";;
       "-a")
	  file="$2"
	  shift;;
       "-d")
	  min="$2"
	  max="$3"
	  shift
	  shift;;   
       *)
	  args="$args $1";;
  esac
  shift 
done

echo "Aquestes són les opcions: $opcions"
echo "Aquests són els arguments: $args"
echo "Aquest és el fitxer: $file"
echo "Aquest és el mínim: $min, i aquest és el màxim: $max"

exit $OK

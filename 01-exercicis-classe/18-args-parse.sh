#! bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Primer s'escriuen les opcions i després els arguments.
# programa [-a file -b -c -dnum -e] arg[...]
# ------------------------------------------

# CONSTANTS:
OK=0

# 1) XIXA :
opcions=""
args=""
file=""
num=""
while [ -n "$1" ]  # -n --> quan hi hagi contingut (no estigui buit) | "$1" (cometes), ja que és un string
do
  case $1 in
       -[bce])
          opcions="$opcions $1";;
       "-a")
	  file="$2"
	  shift;;
       "-d")
	  num="$2"
	  shift;;   
       *)
	  args="$args $1";;
  esac
  shift  # desplaçem l'argument i passem al següent
done

echo "Aquestes són les opcions: $opcions"
echo "Aquests són els arguments: $args"
echo "Aquest és el fitxer: $file"
echo "Aquest és el número: $num"

exit $OK

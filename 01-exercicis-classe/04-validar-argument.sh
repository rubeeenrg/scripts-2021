#! /bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Validar programa té exactament dos args i els mostra.
# $ programa $1 $2
# -------------------------
# 1) Validem arguments:
if [ $# -ne 2 ]
then
  echo "ERROR: nº arguments incorrecte"
  echo "Usage: $0 arg1 arg2"
  exit 1
fi

# 2) Xixa
echo "Els arguments són: $1, $2"
exit 0

#! bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Descripció: Copiar $file a $dir
# programa $file $dir
# Validem que existeix 2 args, el 1r ha de ser un file i el 2n un drectori.
# -----------------------------------

# CONSTANTS:
ERR_NARGS=1
ERR_NOFILE=2
ERR_NODIR=3
OK=0

# 1) Validem que hi hagin 2 arguments:
if [ $# -ne 2 ]
then
  echo "ERROR: nº arguments invàlid"
  echo "Usage: $0 file dir_desti"
  exit $ERR_NARGS
fi

# 2) Validem que existeix el file:
file=$1
if ! [ -e $file ]
then
  echo "ERROR: $file no existeix"
  echo "Usage: $0 file dir_desti"
  exit $ERR_NOFILE
fi

# 3) Validar que és un dir_destí:
dir=$2
if ! [ -d $dir ]
then
  echo "ERROR: $dir no és un directori"
  echo "Usage: $0 file dir_desti"
  exit $ERR_NODIR
fi

# 4) XIXA:
cp $file $dir

exit $OK

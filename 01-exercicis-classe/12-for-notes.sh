#! /bin/bash
# Rubén Rodríguez García
# Febrer 2021
# prog notes [...]
# Validar que rep 1 o més arguments
# Validar que la nota està entre el [0-10]
# Validar per cada nota i dir si és supès, aprobat, notable o excel·lent
#-----------------------------------------------------------------------
# CONSTANTS:
ERR_NARGS=1
OK=0

# 1) Validem arguments:
if [ $# -eq 0 ]
then
  echo "ERROR: nº arguments incorrecte"
  echo "Usage: $0 nota[...]"
  exit $ERR_NARGS
fi

# 2) Xixa:
llista_notes=$*
for nota in $llista_notes
do
  if ! [ $nota -ge 0 -a $nota -le 10 ]
  then
      echo "ERROR: La nota introduïda: $nota, no és correcte." >> /dev/stderr
      echo "La nota pren el valor entre 0 i 10." >> /dev/stderr
  elif [ $nota -lt 5 ] 
  then
      echo "Estàs suspès: $nota"
  elif [ $nota -lt 7 ] 
  then
      echo "Estàs aprovat: $nota"
  elif [ $nota -lt 9 ] 
  then
      echo "Notable: $nota"
  else
      echo "Excel·lent: $nota"
  fi
done

exit $OK

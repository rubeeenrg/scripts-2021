#! bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Descripció: Llistar dir
# programa $dir1 $dir2 [...]
# Validem que existeix 1 o més args i que siguin directoris
# -----------------------------------

# CONSTANTS
ERR_NARGS=1
ERR_NODIR=2
OK=0

# 1) Validem que hi ha un argument:
if [ $# -eq 0 ]
then
   echo "ERROR: nº arguments incorrecte"
   echo "Usage: $0 dir"
   exit $ERR_NARGS
fi

# 2) XIXA:
dir=$1
for dir in $*
do
  if ! [ -d $dir ]
  then
    echo "ERROR: $dir no és un directori" >> /dev/stderr
    exit $ERR_NODIR
  else
    echo "Directori: $dir"
    llista_elements=$(ls $dir)
    for element in $llista_elements
    do
      if [ -h $dir/$element ]
      then
        echo -e "\t $element és un symbolic link"
      elif [ -f $dir/$element ]
      then
        echo -e "\t $element és un regular file"
      elif [ -d $dir/$element ]
      then
        echo -e "\t $element és un directori"
      else
        echo -e "\t $element és un altra cosa."
      fi
    done
   fi
done

exit $OK

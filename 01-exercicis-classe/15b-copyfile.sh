#! bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Descripció: Copiar $file[...] a $dir
# programa $file[...] $dir
# Validem que existeix + de 2 args, el 1r ha de ser un o més file(s) i el 2n un drectori.
# nº args
# llista args
# últim args
# llista_files
# EX: carta treball informe apunts (4 args)
# ------------------------------------------

# CONSTANTS:
ERR_NARGS=1
ERR_NOFILE=2
ERR_NODIR=3
OK=0

# 1) Validem que hi hagin 2 arguments:
if [ $# -lt 2 ]
then
  echo "ERROR: nº arguments invàlid"
  echo "Usage: $0 file[...] dir_desti"
  exit $ERR_NARGS
fi

# 2) Llistem els arguments:
echo $#
echo $*
echo 
echo $* | sed 's/ [^ ]*$//g'
echo $* | sed 's/^.* //g'
echo
echo $* | cut -f1-$(($#-1)) -d' '
echo $* | cut -f$# -d' '

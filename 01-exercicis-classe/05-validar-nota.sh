#! /bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Mostrar si està aprovat o suspès
# Validar que rep un arg i que la nota sigui un valor entre 0-10.
# $ programa $1
# -------------------------
# CONSTANTS:
ERR_NARGS=1
ERR_VALOR=2

# 1) Validem arguments:
if [ $# -ne 1 ]
then
  echo "ERROR: nº arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi

# 2) Validem el valor de la nota
nota=$1
if ! [ $nota -ge 0 -a $nota -le 10 ]
then
  echo "ERROR: La nota introduïda no és la correcte"
  echo "La nota pren el valor entre 0 i 10."
  exit $ERR_VALOR # Un error diferents és un número diferent.
fi

# 3) Xixa
if [ $nota -lt 5 ]
then
  echo "Estàs suspès: $nota"
else
  echo "Estàs aprovat: $nota"
fi

exit 0

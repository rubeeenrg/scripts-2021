#! bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Descripció: Copiar $file[...] a $dir
# programa $file[...] $dir
# Amb un bucle for, validem que existeix + de 2 args, validem per cada file que existeix file i dir i copiar-lo al destí, el 1r ha de ser un o més file(s) i el 23 BLOCSS
# ------------------------------------------

# CONSTANTS:
ERR_NARGS=1
ERR_NODIR=2
ERR_NOREGFILE=3
OK=0

# 1) Validem que hi hagin 2 arguments:
if [ $# -lt 2 ]
then
  echo "ERROR: nº arguments invàlid"
  echo "Usage: $0 file[...] dir_desti"
  exit $ERR_NARGS
fi
desti=$(echo $* | cut -d' ' -f$# )
llistat=$(echo $* | cut -d' ' -f1-$(($#-1)))

# 2) Validem que dir-destí exsiteix:
if [ ! -d $desti ]; then
  echo "ERROR: $desti no és un directori"
  echo "Usage: $0 file dir-destí"
  exit $ERR_NODIR
fi

# 3) XIXA:
for file in $llistaFile
do
  if [ ! -f $file ]; then
    echo "Error: $file no és un regular file" >> /dev/stderr
    echo "usage $0 file[...] dir-destí" >> /dev/stderr
  fi
  cp $file $desti
done

# XIXA
for file in $llistat
do
  if [ ! -f $file ]; then
    echo "ERROR: $file no és un regular file" >> /dev/stderr
    echo "Usage: $0 file[...] dir-destí" >> /dev/stderr
  fi
  cp $file $desti
done

exit $OK

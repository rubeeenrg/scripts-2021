#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 6) Processar per stdin linies d’entrada tipus “Tom Snyder” i mostrar per stdout la línia en format → T. Snyder.
# $prog < [...]
# -----------------------------------------------------------------

# CONSTANTS
OK=0

# 1) XIXA:
while read -r line
do
  nom=$(echo $line | cut -c1)
  cognom=$(echo $line | cut -f2 -d' ')
  echo "$nom. $cognom"
done

exit $OK

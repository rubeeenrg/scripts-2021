#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 4) Processar stdin mostrant per stdout les línies numerades i en majúscules.
# $prog < [...]
# -----------------------------------------------------------------

# CONSTANTS
comptador=0
OK=0

# 1) XIXA:
while read -r line
do
  echo $line | tr '[a-z]' '[A-Z]'
  comptador=$((comptador+1))
done

exit $OK

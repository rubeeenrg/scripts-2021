#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 9) Programa: prog.sh [ -r -m -c cognom  -j  -e edat ]  arg…
#             Escriure el codi que desa en les variables: opcions, cognom, edat i arguments els valors corresponents.
#             No cal validar ni mostrar res!
#             Per exemple si es crida: $ prog.sh -e 18 -r -c puig -j wheel postgres ldap
#             retorna: opcions «-r -j», cognom «puig», edat «18», arguments «wheel postgres ldap»
#
# Validar que hi ha 2 arguments (incolim el flag).
# $prog -r -m -c cognom [...]
# -----------------------------------------------------------------

# CONSTANTS
ERR_NARGS=1
OK=0

# 1) Validem que hi hagin 1 o més arguments
if [ $# -eq 0 ]
then
  echo "ERROR: nº arguments incorrecte."
  echo "Usage: $0 -h/--help file [...]"
  exit $ERR_NARGS
fi

# 2) XIXA: 
opcions=""
cognom=""
args=""
edat=""
while [ -n "$1" ]
do
  case $1 in
       -[rmj])
	  opcions="$opcions $1";;
       "-c")
	  cognom="$2"
	  shift;;
       "-e")
	  edat="$2"
	  shift;;
       *)
	  args="$args $1";;
   esac
   shift 
done

echo "Opcions: $opcions"
echo "Cognoms: $cognom"
echo "Edat: $edat"
echo "Arguments: $args"

exit $OK

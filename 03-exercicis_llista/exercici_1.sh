#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 1) Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
# Validar que hi hagi 1 o més args.
# $prog arg1 [...]
# -----------------------------------------------------------------

# CONSTANTS
ERR_NARGS=1
OK=0

# 1) Validem que hi hagin 1 o més args
if [ $# -eq 0 ]
then
  echo "ERROR: nº arguments incorrecte."
  echo "Usage: $0 arg1 [...]"
  exit $ERR_NARGS
fi

# 2) XIXA
for args in $*
do
  echo $args | egrep '.{4,}'
done

exit $OK

#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 10) Programa: prog.sh 
#                 Rep per stdin GIDs i llista per stdout la informació de cada un d’aquests grups, en format: gname: GNAME, gid: GID, users: USERS
#
# $prog < [...]
# -----------------------------------------------------------------

# CONSTANTS
ERR_NARGS=1
status=0

while read -r line
do
  gline=$(grep "^[^:]*:[^:]*:$line:" /etc/group)
  if [ $? -eq 0 ]; then
    gname=$(echo $gline | cut -d: -f1 | tr '[a-z]' '[A-Z]')
    ulist=$(echo $gline | cut -d: -f4 | tr '[a-z]' '[A-Z]')
    echo "gname: $gname, gid: $line, users: $ulist"
  else
    echo "ERROR: $line no existeix!" >> /dev/stderr
    status=1
  fi
done

exit $status

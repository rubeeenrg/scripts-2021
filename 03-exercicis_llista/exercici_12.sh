#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 12) Programa -h uid…
#             Per a cada uid mostra la informació de l’usuari en format:
#             logid(uid) gname home shell
#
# $prog uid
# -----------------------------------------------------------------

# CONSTANTS:
ERR_NARGS=1
OK=0
status=0

# 1) Validem que hi hagi 1 arg i que la opció introduïda sigui la especificada:
if [ $# -eq 1 -a "$1" = "-h" ]
then
  echo "Menú help..."
  echo "Usage: $0 -h uid [...]"
  exit $OK
fi

if [ $# -eq 0 ]
then
  echo "ERROR: nº arguments incorrecte."
  echo "Usage: $0 -h uid [...]"
  exit $ERR_NARGS
fi

# 2) XIXA:
for uid in $*
do
  uline=$(grep "^[^:]*:[^:]*:$uid:" /etc/passwd)
  if [ $? -eq 0 ]; then
    login=$(echo $uline | cut -f1 -d:)
    gid=$(echo $uline | cut -f4 -d:)
    gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -f1 -d:)
    home_dir=$(echo $uline | cut -f6 -d:)
    shell=$(echo $uline | cut -f7 -d:)
    echo "$login($uid) $gname $home_dir $shell"
  else
    echo "ERROR: $uid no existeix." >> /dev/stderr
  fi
done

exit $status

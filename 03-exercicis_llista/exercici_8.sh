#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 8) Programa: prog file…
#             a)Validar existeix almenys un file. Per a cada file comprimir-lo. Generar per stdout el nom del file comprimit si s’ha comprimit correctament, o un missatge d’error per stderror si no s’ha pogut comprimir. 
#             En finalitzar es mostra per stdout quants files ha comprimit. 
#             Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir.
#             b) Ampliar amb el cas: prog -h|--help.
#
# Validar que hi ha 2 arguments (incolim el flag).
# $prog -h/--help file [...]
# -----------------------------------------------------------------

# CONSTANTS
ERR_NARGS=1
OK=0

# 1) Validem que hi hagin 1 o més arguments
if [ $# -eq 0 ]
then
  echo "ERROR: nº arguments incorrecte."
  echo "Usage: $0 -h/--help file [...]"
  exit $ERR_NARGS
fi

# 2) Validem que el flag introduït sigui l'especificat:
if [ $1 != "-h" -o $1 != "--help" ]
then
  echo "ERROR: no has introduït els flags especificats."
  echo "Usage: $0 -h/--help file"
  exit $OK
fi

# 3) XIXA:
comptador=0
status=0
for file in $*
do
  if [ -f $file ]
  then
    gzip $file &> /dev/null
    if [ $? -eq 0 ]
    then
      echo "$file comprimit correctament."
      comptador=$((comptador+1))
    else
      echo "ERROR: $file no s'ha pogut comprimir." >> /dev/stderr
      status=2
    fi
  else
    echo "ERROR: $file no s'ha pogut comprimir." >> /dev/stderr
    status=2
  fi
done

echo "S'han comprimit $comptador files."
exit $status

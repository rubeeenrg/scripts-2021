#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 3) Processar arguments que són matricules: 
#	      a) llistar les vàlides, del tipus: 9999-AAA. 
#	      b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el número d’errors (de no vàlides)
# Validar que hi hagi 1 o més args.
# $prog matricula [...]
# -----------------------------------------------------------------

# CONSTANTS
ERR_NARGS=1
comptador=0
status=0

# 1) Validem que hi hagin 1 o més args:
if [ $# -eq 0 ]
then
  echo "ERROR: nº arguments incorrecte."
  echo "Usage: $0 matricula [...]"
  exit $ERR_NARGS
fi

# 2) XIXA:
for matricula in $*
do
  echo $matricula | egrep "^[0-9]{4}-[A-Z]{3}$" &> /dev/null
  if [ $? -eq 0 ]
  then
    echo "$matricula"
  else
    echo "$matricula" >> /dev/stderr
    comptador=$((comptador+1))
    status=$comptador
  fi
done

exit $status

#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 5) Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
# $prog < [...]
# -----------------------------------------------------------------

# CONSTANTS
OK=0

# 1) XIXA:
while read -r line
do
  caracters=$(echo $line | wc -c)
  if [ $caracters -lt 50 ]
  then
    echo $line
  fi
done

exit $OK

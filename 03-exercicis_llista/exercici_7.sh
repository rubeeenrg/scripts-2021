#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 7) Programa: prog -f|-d arg1 arg2 arg3 arg4
#	      a) Valida que els quatre arguments rebuts són tots del tipus que indica el flag. És a dir, si es crida amb -f valida que tots quatre són file. Si es crida amb -d valida que tots quatre són directoris.
#	         Retorna 0 ok, 1 error nº args, 2 hi ha elements errònis. 
#                Exemple: prog -f carta.txt a.txt /tmp fi.txt → retorna status 2. 
#              b) Ampliar amb el cas: prog -h|--help.
# Validar que hi ha 5 arguments (incolim el flag).
# $prog -f/-d file1/dir1 [...]
# -----------------------------------------------------------------

# CONSTANTS
ERR_NARGS=1
ERR_FLAGS=2
OK=0
status=0

# 1) Validem que hi ha 5 arguments
if [ $# -ne 5 ]
then
  echo "ERROR: nº arguments incorrecte."
  echo "Usage: $0 -f/-d file1/dir1 [...]"
  exit $ERR_NARGS
fi

# 2) Validem que l'argument sigui l'especificat.
if [ "$1" != "-h" -o "$1" != "--help" ]
then
  echo "ERROR: no has introduït els flags especificats."
  echo "Usage: $0 -f/-d file1/dir1 [...]"
  exit $OK
fi

# 3) Validem que el flag introduït sigui l'especificat:
if [ "$1" != "-f" -a "$1" != "-d" ]
then
  echo "ERROR: no has introduït els flags especificats."
  echo "Usage: $0 -f/-d file1/dir1 [...]"
  exit $ERR_FLAGS
fi

flag=$1

# 4) XIXA:
for arg in $*
do
  if ! [ $flag "$arg" ]; then
    status=2
  fi
done

exit $status

#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 2) Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.
# Validar que hi hagi 1 o més args.
# $prog arg1 [...]
# -----------------------------------------------------------------

# CONSTANTS
ERR_NARGS=1
OK=0

# 1) Validem que hi hagin 1 o més args
if [ $# -eq 0 ]
then
  echo "ERROR: nº arguments incorrecte."
  echo "Usage: $0 arg1 [...]"
  exit $ERR_NARGS
fi

# 2) XIXA
num=0
for args in $*
do
  echo $args | egrep '.{3,}' &> /dev/null
  if [ $? -eq 0 ]
  then
    num=$((num+1))
  fi
done

echo "Hi ha $num de 3 o més caràcters."
exit $OK

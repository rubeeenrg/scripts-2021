#!/bin/bash
# @isx48062351 - Rubén Rodríguez García
# Curs 2020-21
# Juny 2021
# Descripció: Donat un arxiu on cada linea conté un format tipus:  localhost ftp file1 file2 file3
#             Es demana agrupar per schema (protocol/servei) i mostrem el host i els arxius amb linea tabulada
#             El script es podrà cridar d'aquestes formes: prog.sh -h o bé prog.sh --help o bé prog.sh [fitxer]
#

function procesaFitxer() {
  # CONSTANTS:
  OK=0
  file=$1

  # 1) Fem un llistat pel servei:
  llistatServei=$(tr -s '[:blank:]' ' ' < $file | cut -f2 -d' ' | sort -k2 -t' ' | uniq)

  #2) XIXA:
  for service in $llistatServei
  do
    echo $service
    tr -s '[:blank:]' ' ' < $file | egrep "^[^ ]* $service" | sed -r 's/^(.*)$/\t\1/g'
  done

  return $OK
}

# CONSTANTS:
OK=
ERR_NARGS=1
ERR_BAD_ARG=2
ERR_NO_FILE=3

# 1) Validem que és rep 1 argument:
if [ $# -eq 0 ]
then
  echo "ERROR: nº arguments incorrecte."
  echo "Usage: $0 --help or $0 -h or $0 file"
  exit $ERR_NARGS
fi

# 2) Validem si l'argument rebut és "--help" or "-h":
if [ $# -eq 1 -a $1 = "--help" -o $1 = "-h" ]
then
  echo "Mostrant el man..."
  echo "Usage: $0 --help or $0 -h or $0 file"
  exit $ERR_BAD_ARG
fi

# 3) XIXA:
if [ -f "$1" ]
then
  procesaFitxer $1
else
  echo "$1 no existeix."
  echo "Usage: $0 --help or $0 -h or $0 file"
  exit $ERR_NO_FILE
fi

exit $OK

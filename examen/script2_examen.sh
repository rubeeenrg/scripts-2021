#!/bin/bash
# @isx48062351 - Rubén Rodríguez García
# Curs 2020-21
# Juny 2021
# Descripció: Amb un arxiu en una línea tipus: ssh user:host file[...]
#	      Copiar els arxius locals (especificats després del host) en el home de l'usuari que escribim.
#	      
# Exmple usage: ssh guest:j22 file[...]
#               Copiarà fitxerExamen en el home de guest@j22 i copiarà a.html en el home de rubeeenrg@localhost
#
# Per fer les accions utilitzarem: sshpass -p jupiter ssh root@localhost
#				   sshpass -p pass_rubeenrg scp a.html rubeeenrg@localhost
#

# CONSTANTS:
OK=0
ERR_NO_FILE=1
file=/dev/stdin

# 1) Comprovem si hi ha un argument:
if [ $# -eq 1 ]
then
  file=$1
fi

# 2) Comprovem si $file és un fitxer:
if [ ! -f "$file" ]
then
  echo "ERROR: $file inexistent."
  exit $ERR_NO_FILE
fi

# 3) XIXA:
while read -r line
do
  user=$(echo $line | cut -f2 -d' ' | cut -f1 -d:)
  host=$(echo $line | cut -f2 -d' ' | cut -f2 -d:)
  files_list=$(echo $line | sed -r 's/^[^ ]* [^ ]* (.*)$/\1/')
  #echo "sshpass -p guest scp $files_list $user@$host:"
  sshpass -p guest scp $files_list $user@$host:
done < $file

exit $OK

#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 10. Fer un programa que rep com a argument un número indicatiu del número màxim de línies a mostrar. El programa processa stdin línia a línia i mostra numerades un màxim de num línies.
# $ programa usuari1 [...]
# -----------------------------------------------------------------

# CONSTANTS:
ERR_NARGS=1
OK=0

# 1) Validem que hi ha un argument:
if [ $# -ne 1 ] 
then
  echo "ERROR: nº arguments incorrecte"
  echo "Usage: $0 usuari1 [...]"
  exit $ERR_NARGS
fi

# 2) XIXA:
MAX=$1
num=1
while read -r line
do
  if [ "$num" -le $MAX ]
  then
    echo "$num: $line"
  else
    echo "$line"
  fi
  num=$((num+1))
done

exit $OK

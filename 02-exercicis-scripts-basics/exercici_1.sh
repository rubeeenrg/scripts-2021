#! bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Descripció: 1. Mostrar l’entrada estàndard numerant línia a línia
# prog < [...]
# -----------------------------------------------------------------
num=1
while read -r line
do
  echo "$num: $line"
  ((num++))
done

exit 0

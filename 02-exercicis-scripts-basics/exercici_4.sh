#! bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Descripció: 4. Fer un programa que rep com a arguments números de mes (un o més) i indica per a cada mes rebut quants dies té el més.
# prog $mes[...]
# -----------------------------------------------------------------

# CONSTANTS
ERR_NARGS=1
OK=0

# 1) Validem que hi ha com a mínim un argument:
if [ $# -eq 0 ]
then
  echo "ERROR: nº arguments incorrecte"
  echo "Usage: $0 mes[...]"
  exit $ERR_NARGS
fi

# 2) Xixa:
for mes in $*
do
  if ! [ $mes -ge 1 -a $mes -le 12 ]
  then
     echo "ERROR: Valor de l'argument no vàlid." >> /dev/stderr
     echo "Usage: $0 mes (valor de l'1 al 12)." >> /dev/stderr
  else
    case $mes in
        "2")
          dies=28;;
        "4"|"6"|"9"|"11")
          dies=30;;
        *)
          dies=31;;
    esac
  echo "El mes: $mes, te $dies díes."
  fi
done

exit $OK

#! bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Descripció: 5. Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters.
# prog < [...]
# -----------------------------------------------------------------

# CONSTANTS:
OK=0

# 1) XIXA:
while read -r line
do
  echo $line | cut -c1-50
done

exit $OK

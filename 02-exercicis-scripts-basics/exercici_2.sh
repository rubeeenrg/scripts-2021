#! bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Descripció: 2. Mostar els arguments rebuts línia a línia, tot numerànt-los.
# prog $arg [...]
# -----------------------------------------------------------------

# CONSTANTS
ERR_NARGS=1
OK=0

# 1) Validem que hi ha com a mínim un argument:
if [ $# -eq 0 ]
then
   echo "ERROR: nº arguments incorrecte"
   echo "Usage: $0 arg[...]"
   exit $ERR_NARGS
fi

# 2) XIXA:
num=1
for arg in $*
do
  echo "$num: $arg"
  ((num++))
done

exit $OK

#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 9. Fer un programa que rep per stdin noms d’usuari (un per línia), si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.
# $ programa < [...]
# -----------------------------------------------------------------

# CONSTANTS:
OK=0

# 1) XIXA:
comptador=0
while read -r user
do
  egrep "^$user:" /etc/passwd
  if [ $? -ne 0 ]
  then
    echo $user >> /dev/stderr
    comptador=$((comptador+1))
  fi
done

echo "Aquests usuaris no estàn al sistema: $comptador"
exit $OK

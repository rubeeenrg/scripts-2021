#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 8. Fer un programa que rep com a argument noms d’usuari, si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.
# $ programa usuari1 [...]
# -----------------------------------------------------------------

# CONSTANTS:
ERR_NARGS=1
OK=0

# 1) Validem que hi hagin 1 o més args:
if [ $# -eq 0 ]
then
  echo "ERROR: nº arguments incorrectes"
  echo "Usage: $0 usuari1 [...]"
  exit $ERR_NARGS
fi

# 2) XIXA:
for nom in $*
do
  egrep "^$nom:" /etc/passwd
  if [ $? -eq 0 ]
  then
    echo "$nom"
  else
    echo $nom >> /dev/stderr
  fi
done

exit $OK

#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 6. Fer un programa que rep com a arguments noms de dies de la setmana i mostra quants dies eren laborables i quants festius. Si l’argument no és un dia de la setmana genera un error per stderr.
# $ prog dl dt dm [...] 
# -----------------------------------------------------------------

# CONSTANTS:
ERR_NARGS=1
OK=0

# 1) Validem que hi ha 1 o més args:
if [ $# -eq 0 ]
then
  echo "ERROR: nº arguments incorrecte"
  echo "Usage: $0 dl dm [...]"
  exit $ERR_NARGS
fi

# 2) XIXA:
laborable=0
festiu=0

for dies in $*
do
  case $dies in
       "dl"|"dt"|"dm"|"dj"|"dv")
	 laborable=$((laborable+1));;
       "ds"|"dm")
	 festiu=$((festiu+1));; 
       *)
	 echo "$dies no és un dia, proba amb: dl, dm..." >> /dev/stderr
   esac
done

echo "Díes laborables introduïts: $laborable"
echo "Díes festius introduïts: $festiu"

exit $OK

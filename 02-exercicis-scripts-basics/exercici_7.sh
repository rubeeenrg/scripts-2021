#! bin/bash
# Rubén Rodríguez García
# Març 2021
# Descripció: 7. Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la mostra, si no no.
# $ programa < [...]
# -----------------------------------------------------------------

# CONSTANTS:
OK=0

# 1) XIXA:
while read -r line
do
  num=$(echo "$line" | wc -c)
  if [ "$num" -gt 60 ]
  then
    echo $line
  fi
done

exit $OK

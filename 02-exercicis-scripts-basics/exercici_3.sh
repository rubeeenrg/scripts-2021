#! bin/bash
# Rubén Rodríguez García
# Febrer 2021
# Descripció: 3. Fer un comptador des de zero fins al valor indicat per l’argument rebut.
# prog $num
# -----------------------------------------------------------------

# CONSTANTS
ERR_NARGS=1
OK=0

# 1) Validem que hi ha un argument:
if [ $# -ne 1 ]
then
   echo "ERROR: nº arguments incorrecte"
   echo "Usage: $0 arg[...]"
   exit $ERR_NARGS
fi

# 2) XIXA:
num=0
MAX=$1
while [ $num -le $MAX ]
do
  echo "$num"
  ((num++))
done

exit $OK

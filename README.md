# M01 ISO Operatius
## @edt Curs 2020-2021 - Rubén Rodríguez García

### Exercicis scripts:

**01-exercicis-classe** --> Exercicis d'explicació.

**02-exercicis-scripts-basics** --> Exercicis de repàs bàsics.

**03-exercicis_llista** --> Exercicis de repàs de llistes.

**04-usuaris** --> Exercicis de repàs de creació i administració d'usuris.

**05-disc** --> Exercicis de repàs de disc.

**examen** --> Exercicis d'exàmen UF4 (scp, ssh...)

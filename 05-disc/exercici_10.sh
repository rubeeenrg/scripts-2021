# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Abril 2021
# Descripció: Llista per a cada fstype que existeix al fstab (per ordre lexicogràfic)
#             les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.
#

function allfstype() {   
  status=0   

  fstypeList=$(egrep -v "^#" /etc/fstab | tr -s '[:blank:]' ' ' | cut -d' ' -f3 | sort -u)   

  for fstype in $fstypeList   
  do     
    echo $fstype
    fstype $fstype   
  done   

  return $status 
}

function fstype(){
  # CONSTANTS:
  status=0

  # 1) Validem que hi hagi 1 argument:
  if [ $# -ne 1  ]
  then
    echo "ERROR: nº arguments incorrecte"
    echo "Usage: $0 fstype"
    return $ERR_NARGS
  fi

  # 2) XIXA:
  fstype=$1
  egrep -v "^#" /etc/fstab | tr -s '[:blank:]' ' ' | egrep "^[^ ]* [^ ]* $fstype " | cut -f1,2 -d' ' | sed -r 's/^(.*)$/\t\1/'
  return $status
}

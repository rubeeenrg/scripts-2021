# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Maig 2021
# Descripció: 11) LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic) les entrades d'aquest tipus. 
#                 Llistar tabuladament el device i el mountpoint. Es rep un valor numèric d'argument que indica el número
#                 mínim d'entrades d'aquest fstype que hi ha d'haver per sortir al llistat
#

function allfstypeif(){
  # CONSTANTS:
  ERR_NARGS=1
  status=0

  # 1) Validem que es rep 1 argument:
  if [ $# -ne 1  ]
  then
    echo "ERROR: nº arguments incorrecte."
    echo "Usage: $0 fstype"
    return $ERR_NARGS
  fi

  # 2) XIXA:
  MIN=$1
  fstypeList=$(egrep -v "^#" /etc/fstab | tr -s '[:blank:]' ' ' | cut -f3 -d' ' | sort -u)
  for fstype in $fstypeList
  do
    entrades=$(egrep -v "^#" /etc/fstab | tr -s '[:blank:]' ' ' | egrep "^[^ ]* [^ ]* $fstype " | wc -l)
    if [ $entrades -ge $MIN ]
    then	      
      fstype $fstype
    fi  
  done
  return $status
}

function fstype(){
  # CONSTANTS:
  ERR_NARGS=1
  status=0

  # 1) Validem que hi hagi 1 argument:
  if [ $# -ne 1  ]
  then
    echo "ERROR: nº arguments incorrecte"
    echo "Usage: $0 fstype"
    return $ERR_NARGS
  fi

  # 2) XIXA:
  fstype=$1
  egrep -v "^#" /etc/fstab | tr -s '[:blank:]' ' ' | egrep "^[^ ]* [^ ]* $fstype " | cut -f1,2 -d' ' | sed -r 's/^(.*)$/\t\1/'
  return $status
}

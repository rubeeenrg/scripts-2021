# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Abril 2021
# Descripció: Rep com a argument un nom de fitxer que conté un lògin per línia. Mostrar l'ocupació de disc de cada
#             usuari usant fsize. Verificar que es rep un argument i que és un regular file. 
#

function loginfile() {
  # CONSTANTS:
  ERR_NARGS=1
  status=0

  # 1) Verifiquem que es rep 1 argument:
  if [ $# -ne 1 ]
  then
    echo "ERROR: nº arguments incorrecte."
    echo "Usage: $0 fitxer"
    return $ERR_NARGS
  fi

  # 2) XIXA:
  file=$1
  while read -r login
  do
    fsize $login
  done < $file

  return $status
}

function fsize() {
  # CONSTANTS:
  ERR_NO_LOGIN=1
  status=0

  # 1) XIXA:
  login=$1
  homeDir=""
  homeDir=$(egrep "^$login:" /etc/passwd | cut -f6 -d:)
  if [ -z "$homeDir" ]
  then
    echo "ERROR: $login no vàlid."
    echo "Usage: $0 login."
    return $ERR_NO_LOGIN
  fi

  du -hs $homeDir

  return $status
}

# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Maig 2021
# Descripció: Escriu un programa que rep com a argument el nom d'un fitxer i una llista de ID-fstype (valor numèric ID
#	      identificador del sistema de fitxers). El fitxer conté un llistat tabulat tipus fdisk on el 5è camp és la mida i el sisè
#             el ID-fstype. Per cada ID-fstype rebut d'argument llistar: ID-fstype, count i size. És a dir, l'identificador ID-fstype,
#             el número d'entrades que hi ha d'aquest tipus i quant sumen les seves mides.
#

function ID-fstype() {
  # CONSTANTS:
  OK=0
  
  # 1) Inicialitzem valors:
  fileIN=$1
  shift
  
  # 2) XIXA:
  for fsID in $*
  do
    num=$(tr -s '[:blank:]' ':' < $fileIN | cut -f5,6 -d: | egrep ":$fsID$" | wc -l)
    llista_mides=$(tr -s '[:blank:]' ':' < $fileIN | cut -f5,6 -d: | egrep ":$fsID$" | cut -f1 -d:)
    midaTotal=0
    for unaMida in $llista_mides
    do
      midaTotal=$((midaTotal+unaMida))
    done
    echo "$fsID: $num $midaTotal"
  done

  return $OK
}

# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Abril 2021
# Descripció: Donat un fstype llista el device i el mountpoint (per odre de device) de les entrades de fstab d'aquest fstype.
#

function fstype(){
  # CONSTANTS:
  ERR_NARGS=1
  status=0

  # 1) Validem que hi hagi 1 argument:
  if [ $# -ne 1  ]
  then
    echo "ERROR: nº arguments incorrecte"
    echo "Usage: $0 fstype"
    return $ERR_NARGS
  fi

  # 2) XIXA:
  fstype=$1
  egrep -v "^#" /etc/fstab | tr -s '[:blank:]' ' ' | egrep "^[^ ]* [^ ]* $fstype " | cut -f1,2 -d' ' | sed -r 's/^(.*)$/\t\1/'
  return $status
}

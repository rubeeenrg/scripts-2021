# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Abril 2021
# Descripció: Rep com a argument un file o res (en aquest cas es processa stdin). El fitxer o stdin contenen un lògin per línia.
#             Mostrar l'ocupació de disc del home de l'usuari. Verificar els arguments rebuts. 
#             Verificar per cada login rebut que és vàlid.
#

function loginboth() {
  file=/dev/stdin
  # 1) Si es rep 1 argument:
  if [ $# -eq 1 ] 
  then
    file=$1
  fi

  # 2) Si no rebem cap argument:
  while read -r login
  do
    fsize $login
  done < $file
}

function fsize() {
  # CONSTANTS:
  ERR_NO_LOGIN=1
  status=0

  # 1) XIXA:
  login=$1
  homeDir=""
  homeDir=$(egrep "^$login:" /etc/passwd | cut -f6 -d:)
  if [ -z "$homeDir" ]
  then
    echo "ERROR: $login no vàlid."
    echo "Usage: $0 login."
    return $ERR_NO_LOGIN
  fi

  du -hs $homeDir

  return $status
}

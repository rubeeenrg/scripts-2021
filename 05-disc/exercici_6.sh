# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Maig 2021
# Descripció: 6) Donat un GID com a argument, mostrar per a cada usuari que pertany a aquest grup l'ocupació de disc del seu home. Verificar que es rep un argument i que és un gID vàlid.
#

function gidsize() {
  # CONSTANTS:
  ERR_NARGS=1
  status=0

  # Verifiquem que es rep 1 argument:
  if [ $# -ne 1 ]
  then
    echo "ERROR: nº arguments incorrecte."
    echo "Usage: $0 GID"
    return $ERR_NARGS
  fi

  # 2) XIXA:
  llistaLogins=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -f1 -d: | sort)
  for login in $llistaLogins
  do
    fsize $login	
  done

  return $status
}

function fsize() {
  # CONSTANTS:
  ERR_NO_LOGIN=1
  status=0

  # 1) XIXA:
  login=$1
  homeDir=""
  homeDir=$(egrep "^$login:" /etc/passwd | cut -f6 -d:)
  if [ -z "$homeDir" ]
  then
    echo "ERROR: $login no vàlid."
    echo "Usage: $0 login."
    return $ERR_NO_LOGIN
  fi

  du -hs $homeDir

  return $status
}

# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Abril 2021
# Descripció: Donat un login calcular amb du l'ocupació del home de l'usuari. 
#             Cal obtenir el home del /etc/passwd.
#

function fsize() {
  # CONSTANTS:
  ERR_NO_LOGIN=1
  status=0

  # 1) XIXA:
  login=$1
  homeDir=""
  homeDir=$(egrep "^$login:" /etc/passwd | cut -f6 -d:)
  if [ -z "$homeDir" ]
  then
    echo "ERROR: $login no vàlid."
    echo "Usage: $0 login."
    return $ERR_NO_LOGIN
  fi

  du -hs $homeDir

  return $status
}

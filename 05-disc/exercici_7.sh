# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Maig 2021
# Descripció: 7) Llistar de tots els GID del sistema (en ordre creixent) l'ocupació del home dels usuaris que hi pertanyen.
#

function allgidsize(){
  llistaGids=$(cut -f4 -d: /etc/passwd | sort -g | uniq)
  for gid in $llistaGids
  do
    gidsize $gid 
  done  
}

function gidsize() {
  # CONSTANTS:
  ERR_NARGS=1
  status=0

  # Verifiquem que es rep 1 argument:
  if [ $# -ne 1 ]
  then
    echo "ERROR: nº arguments incorrecte."
    echo "Usage: $0 GID"
    return $ERR_NARGS
  fi

  # 2) XIXA:
  llistaLogins=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -f1 -d: | sort)
  for login in $llistaLogins
  do
    fsize $login	
  done
}

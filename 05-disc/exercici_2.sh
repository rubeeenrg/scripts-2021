# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Abril 2021
# Descripció: Aquesta funció rep logins i per a cada login es mostra l'ocupació de disc del home de l'usuari usant fsize. 
#             Verificar que es rep almenys un argument. Per a cada argument verificar si és un login vàlid, si no genera una traça d'error. 
#

function loginargs() {
  # CONSTANTS:
  ERR_NARGS=1

  # 1) Validem que hi ha mínim 1 argument:
  if [ $# -ne 1 ]
  then
    echo "ERROR: nº arguments incorrecte."
    echo "Usage: $0 login [...]"
    return $ERR_NARGS
  fi

  # 2) XIXA:
  for login in $*
  do
    fsize $login
  done
}

function fsize() {
  # CONSTANTS:
  ERR_NO_LOGIN=1
  status=0

  # 1) XIXA:
  login=$1
  homeDir=""
  homeDir=$(egrep "^$login:" /etc/passwd | cut -f6 -d:)
  if [ -z "$homeDir" ]
  then
    echo "ERROR: $login no vàlid."
    echo "Usage: $0 login."
    return $ERR_NO_LOGIN
  fi

  du -hs $homeDir

  return $status
}

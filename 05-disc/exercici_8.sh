# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Maig 2021
# Descripció: 8) Llistar totes les línies de /etc/group i per cada llínia llistar l'ocupació del home dels usuaris que hi pertanyen. Ampliar filtrant només els grups del 0 al 100
#

function allgroupsize(){
  while read -r line
  do
    gid=$(echo $line | cut -d: -f3)
    if [ $gid -ge 0 -a $gid -le 100 ]
    then
      gidsize $gid
    fi
  done < /etc/group
}

function gidsize() {
  # CONSTANTS:
  ERR_NARGS=1
  status=0

  # Verifiquem que es rep 1 argument:
  if [ $# -ne 1 ]
  then
    echo "ERROR: nº arguments incorrecte."
    echo "Usage: $0 GID"
    return $ERR_NARGS
  fi

  # 2) XIXA:
  llistaLogins=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -f1 -d: | sort)
  for login in $llistaLogins
  do
    fsize $login	
  done
}

# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Abril 2021
# Descripció: Exemples de processament.
#

# Processar línea a línea amb aquestes numerades de stdin o argument.
# 1 argument --> fitxer, 0 arguments --> stdin (PER DEFECTE stdin)
function exemple_filtra_GID() {
  # 1) XIXA:
  file=/dev/stdin
  num=0
  if [ $# -eq 1 ] # Li donem vlaor de $1 en el cas de que l'usuari passi l'argument.
  then
    file=$1
  fi
  
  while read -r login
  do
    echo "$num: $login"
    num=$((num+1))
  done < $file
}

# Processar línea a línea amb aquestes numerades, validant que hi ha 1 argument de tots els usuaris 
# que tinguin un GID més gran o igual a 500:
function exemple_filtra_GID() {
   # CONSTANTS:
   ERR_NARGS=1

   # 1) Validem que rep 1 arg:
   if [ $# -ne 1 ]
   then
     echo "ERROR: nº arguments incorrecte."
     echo "Usage: $0 fitxer"
     return $ERR_NARGS
   fi
 
   # 2) XIXA:
   file=$1
   MAX=500
   while read -r line
   do
     gid=$(echo $line | cut -f4 -d:)
     if [ $gid -ge $MAX ]
     then
       echo "$line"
     fi
   done < $file
}

# Processar línea a línea amb aquestes numerades i validant que hi ha 1 argument:
function exemple_llistar_valid() {
  # CONSTANTS:
  ERR_NARGS=1
  
  # 1) Validem que rep 1 arg:
  if [ $# -ne 1 ]
  then
    echo "ERROR: nº arguments incorrecte."
    echo "Usage: $0 login"
    return $ERR_NARGS
  fi

  # 2) XIXA:  
  file=$1
  num=0
  while read -r login
  do
    echo "$num: $login"
    num=$((num+1))
  done < $file
}


# Processar línea a línea amb aquestes numerades:
function exemple_llistar_numerades() {
  file=$1
  num=0
  while read -r login
  do
    echo "$num: $login"
    num=$((num+1))
  done < $file
}

# Processar línea a línea:
function exemple_llistar() {
  file=$1
  while read -r login
  do
    echo $login
  done < $file
}

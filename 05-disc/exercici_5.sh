# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Maig 2021
# Descripció: 5) Donat un GID com a argument, llistar els logins dels usuaris que petanyen a aquest grup com a grup principal. Verificar que es rep un argument i que és un GID vàlid.
#

function grepgid() {
  # CONSTANTS:
  ERR_NARGS=1
  ERR_NO_GID=2
  status=0

  # 1) Validem que és rep 1 argument:
  if [ $# -ne 1 ]
  then
    echo "ERROR: nº arguments incorrecte."
    echo "Usage: $0 GID"
    return $ERR_NARGS
  fi

  # 2) XIXA:
  gid=$1
  egrep "^[^:]*:[^:]*:$gid:" /etc/group &> /dev/null
  if [ $? -ne 0 ]
  then
    echo "ERROR: $gid no vàlid"
    return $ERR_NO_GID
  fi
  
  egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -f1 -d:

  return $status
}


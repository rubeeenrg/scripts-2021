# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Març 2021
# Descripció: Rebut un login per argument mostrar camp a camp les dades de l’usuari (etiqueta
#	      i valor del camp). Validar que es rep almenys un argument. Validar que és un login vàlid.
#

function show_user(){
  # CONSTANTS:
  ERR_NARGS=1
  ERR_NO_LOGIN=2
  status=0
  
  # 1) Validem que hi ha 1 argument:
  if [ $# -ne 1 ]
  then
    echo "ERROR: nº arguments incorrecte."
    echo "Usage: $0 login"
    return $ERR_NARGS
  fi

  # 2) Validem que existeix l'usuari
  user=$1  
  userLine=""
  userLine=$(egrep "^$user:" /etc/passwd 2> /dev/null)
  if [ -z "$userLine" ]
  then
    echo "ERROR: login $user no existeix."
    echo "Usage: $0 login."
    return $ERR_NO_LOGIN
  fi

  # 3) Mostrem:
  login=$(echo $userLine | cut -f1 -d:)
  x=$(echo $userLine | cut -f2 -d:)
  uid=$(echo $userLine | cut -f3 -d:)
  gid=$(echo $userLine | cut -f4 -d:)
  gecos=$(echo $userLine | cut -f5 -d:)
  homeDir=$(echo $userLine | cut -f6 -d:)
  shell=$(echo $userLine | cut -f7 -d:)
  
  echo "Login: $login"
  echo "x: $x"
  echo "UID: $uid"
  echo "GID: $gid"
  echo "GECOS: $gecos"
  echo "Home dir: $homeDir"
  echo "Shell: $shell"
  return $status
}

# Funció "date":
function dia(){
  date
  return 0
}

# Funció per sumar:
function suma(){
  echo "La suma és: $(($1+$2))"
  return 0
}

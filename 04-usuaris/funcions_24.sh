# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Abril 2021
# Descripció: Ídem exercici anterior però processa un a un els logins que rep per stdin, cada línia un lògin. Valida que el login existeixi, sino, és un error.
#	      Donat el login mostra per stdout el size del seu home. Cal usar les funcions que estem definint.
#

function getSizeIn() {
  # Constants
  status=0

  # 1) XIXA:
  while read -r login
  do
    getHoleList $login &> /dev/null
    if [ $? -eq 0 ]
    then
      homedir=$(getHome $login)
      getSize $homedir
    else
      status=$((status+1))
    fi
  done
  return $status
}

function getSize() {
  homedir=$1
  if [ -d $homedir ]
  then
    du -s -b $homedir | cut -f1
    return 0
  else
    return 1
  fi
}

function getHoleList() {
  # Constants:
  ERR_NARGS=1
  status=0

  # 1) Validem nº arguments:
  if [ $# -eq 0 ]
  then
    echo "ERROR: nº arguments incorrecte." >> /dev/stderr
    echo "Usage: $0 login [...]" >> /dev/stderr
    return $ERR_NARGS
  fi

  for login in $*
  do
    getHome $login
    if [ $? -ne 0 ]
    then
      status=$((status+1))
    fi
  done
  return $status
}

function getHome() {
  login=$1
  egrep "^$login:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]
  then
    egrep "^$login:" /etc/passwd | cut -f6 -d:
    return 0
  else
    return 1
  fi
}

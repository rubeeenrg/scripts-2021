# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Març 2021
# Descripció: Modificar la funció showUser feta anteriorment per fer que mostri també el gname de l’usuari.
#

# CONSTANTS:
ERR_NARGS=1
ERR_NO_LOGIN=2

function show_user_gname(){
  # CONSTANTS:
  ERR_NARGS=1
  ERR_NO_LOGIN=2
  status=0

  # 1) Validem que hi ha 1 argument:
  if [ $# -ne 1 ]
  then
    echo "ERROR: nº arguments incorrecte."
    echo "Usage: $0 login"
    return $ERR_NARGS
  fi

  # 2) Validem que existeix l'usuari
  user=$1  
  userLine=""
  userLine=$(egrep "^$user:" /etc/passwd 2> /dev/null)
  if [ -z "$userLine" ]
  then
    echo "ERROR: login $user no existeix."
    echo "Usage: $0 login."
    return $ERR_NO_LOGIN
  fi

  # 3) Mostrem:
  login=$(echo $userLine | cut -f1 -d:)
  x=$(echo $userLine | cut -f2 -d:)
  uid=$(echo $userLine | cut -f3 -d:)
  gid=$(echo $userLine | cut -f4 -d:)
  gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -f1 -d:)
  gecos=$(echo $userLine | cut -f5 -d:)
  homeDir=$(echo $userLine | cut -f6 -d:)
  shell=$(echo $userLine | cut -f7 -d:)
  
  echo "Login: $login"
  echo "x: $x"
  echo "UID: $uid"
  echo "GID: $gid"
  echo "GNAME: $gname"
  echo "GECOS: $gecos"
  echo "Home dir: $homeDir"
  echo "Shell: $shell"
  return $status
}

# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Abril 2021
# Descripció: Fer una funció que rep un login (no cal validar que rep un argument ni que sigui un login vàlid) i mostra per stdout el home de l’usuari.
#             La funció retorna 0 si ha pogut trobar el home i diferent de zero si no l’ha pogut trobar.
#

function getHome {
  login=$1
  egrep "^$login:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]
  then
    egrep "^$login:" /etc/passwd | cut -f6 -d:
    return 0
  else
    return 1
  fi
}

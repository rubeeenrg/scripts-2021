# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Abril 2021
# Descripció: Llistar per ordre de gname tots els grups del sistema, per a cada grup hi ha una capçalera amb el nom del grup i a continuació, 
#             el llistat de tots els usuaris que tenen aquell grup com a grup principal, ordenat per login.
#

function showAllGroupMembers(){
  # 1) Retallem el gid:
  llista_gids=$(cut -f4 -d: /etc/passwd | sort -nu)
  for gid in $llista_gids
  do
    count=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | wc -l)
    if [ $count -ge 2 ]
    then
      gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -f1 -d:)
      # 2) Mostrem:
      echo "$gname($gid): $count"
      egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,7 | sort | sed -r 's/^(.*)$/\t\1/' 
    fi
  done
}

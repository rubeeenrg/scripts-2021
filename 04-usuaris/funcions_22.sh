# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Abril 2021
# Descripció: Fer una funció que rep un o més logins d’arguments i per a cada un d’ells mostra el seu home utilitzant la funció getHome. Cal validar els arguments rebuts.
#

function getHoleList() {
  # Constants:
  ERR_NARGS=1
  status=0

  # 1) Validem nº arguments:
  if [ $# -eq 0 ]
  then
    echo "ERROR: nº arguments incorrecte." >> /dev/stderr
    echo "Usage: $0 login [...]" >> /dev/stderr
    return $ERR_NARGS
  fi
  
  for login in $*
  do
    getHome $login
    if [ $? -ne 0 ]
    then
      status=$((status+1))
    fi
  done
  return $status
}

function getHome() {
  login=$1
  egrep "^$login:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]
  then
    egrep "^$login:" /etc/passwd | cut -f6 -d:
    return 0
  else
    return 1
  fi
}

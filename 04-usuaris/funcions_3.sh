# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Març 2021
# Descripció: Rebut un gname per argument mostrar camp a camp les dades del grup (etiqueta
#	      i valor del camp). Validar que es rep almenys un argument. Validar que és un gname vàlid.
# 

function show_group(){
  # CONSTANTS:
  ERR_NARGS=1
  ERR_NO_GROUP=2
  status=0

  # 1) Validem que hi ha 1 argument:
  if [ $# -ne 1 ]
  then
    echo "ERROR: nº arguments incorrecte."
    echo "Usage: $0 group"
    return $ERR_NARGS
  fi

  # 2) Validem que existeix el grup:
  gname=$1
  groupLine=""
  groupLine=$(egrep "^$gname:" /etc/group 2> /dev/null)
  if [ -z "$groupLine" ]
  then
    echo "ERROR: group $group no existeix"
    echo "Usage: $0 group."
    return $ERR_NO_GROUP
  fi
  
  # 3) XIXA:
  x=$(echo $groupLine | cut -f2 -d:)
  gid=$(echo $groupLine | cut -f3 -d:)
  uid=$(echo $groupLine | cut -f4 -d:)
  
  echo "Gname: $gname"
  echo "x: $x"
  echo "GID: $gid"
  echo "UID: $uid"
  return $status
}

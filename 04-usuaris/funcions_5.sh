# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Març 2021
# Descripció: Realitza el mateix que la funció showUser però processa n lògins que rep com a argument. Validar que el login és vàlid i que n’hi ha almenys un.
#	      Atenció, feu tot el codi de nou, sense cridar per res la funció showUser.
#

# CONSTANTS:
ERR_NARGS=1

function show_user_list(){
  # CONSTANTS:
  ERR_NARGS=1
  status=0
  
  # 1) Validem que hi ha 1 argument com a mínim:
  if [ $# -eq 0 ]
  then
    echo "ERROR: nº arguments incorrecte."
    echo "Usage: $0 login [...]"
    return $ERR_NARGS
  fi

  # 2) Llegim els logins:
  for user in $*
  do
    userLine=$(egrep "^$user:" /etc/passwd 2> /dev/null)
    if [ -z "$userLine" ]
    then
      echo "ERROR: login $user no existeix." >> /dev/stderr
      echo "Usage: $0 login [...]"
      status=$((status+1))
      echo ""
    else
      # 3) XIXA:
      login=$(echo $userLine | cut -f1 -d:)
      x=$(echo $userLine | cut -f2 -d:)
      uid=$(echo $userLine | cut -f3 -d:)
      gid=$(echo $userLine | cut -f4 -d:)
      gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
      gecos=$(echo $userLine | cut -f5 -d:)
      homeDir=$(echo $userLine | cut -f6 -d:)
      shell=$(echo $userLine | cut -f7 -d:)
      
      echo "Login: $login"
      echo "x: $x"
      echo "UID: $uid"
      echo "GID: $gid"
      echo "GNAME: $gname"
      echo "GECOS: $gecos"
      echo "Home dir: $homeDir"
      echo "Shell: $shell"
      echo ""
    fi
  done
  return $status
}

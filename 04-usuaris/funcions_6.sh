# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Març 2021
# Descripció: Mostra la informació del usuari tal i com feia showUser o showUserList però processa l’entrada estàndard. Cada línia de l’entrada és un login.
#

function show_user_stdin(){
  # CONSTANTS:
  status=0
  
  # 1) Processem per stdin els logins:
  while read -r user
  do
    userLine=$(egrep "^$user:" /etc/passwd 2> /dev/null)
    if [ -z "$userLine" ]
    then
      echo "ERROR: login $user no existeix." >> /dev/stderr
      echo "Usage: $0 login [...]"
      status=$((status+1))
      echo ""
    else
      # 3) XIXA:
      login=$(echo $userLine | cut -f1 -d:)
      x=$(echo $userLine | cut -f2 -d:)
      uid=$(echo $userLine | cut -f3 -d:)
      gid=$(echo $userLine | cut -f4 -d:)
      gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
      gecos=$(echo $userLine | cut -f5 -d:)
      homeDir=$(echo $userLine | cut -f6 -d:)
      shell=$(echo $userLine | cut -f7 -d:)
      
      echo "Login: $login"
      echo "x: $x"
      echo "UID: $uid"
      echo "GID: $gid"
      echo "GNAME: $gname"
      echo "GECOS: $gecos"
      echo "Home dir: $homeDir"
      echo "Shell: $shell"
      echo ""
    fi
  done
  return $status
}

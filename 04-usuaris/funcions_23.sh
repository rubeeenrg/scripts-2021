# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Abril 2021
# Descripció: Donat un homedir com a argument mostra per stdout l’ocupació enbytes del directori. Per calcular l’ocupació utilitza "du" del que només volem el resultat numèric.
#	      Cal validar que el directori físic existeix(recordeu que hi ha usuaris que poden tenir de home valors que no són rutes vàlides com/bin/false).
#	      Retorna 0 si el directori existeix i un valor diferent si no existeix.
#

function getSize() {
  homedir=$1
  if [ -d $homedir ]
  then
    du -s -b $homedir | cut -f1
    return 0
  else
    return 1
  fi
}

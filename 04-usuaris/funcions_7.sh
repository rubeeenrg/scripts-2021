# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Març 2021
# Descripció: Donat un gname mostrar el llistat dels usuaris que tenen aquest grup com a grup principal. 
#	      Com a llistat mostrar el login, uid, dir i shell dels usuaris (en un format de llistat del grep). 
#	      Validar que es rep un argument i que el gname és vàlid.
#

# CONSTANTS:
ERR_NARGS=1
ERR_NO_GNAME=2

function show_group_members(){
  # 1) Validem que hi ha 1 argument:
  if [ $# -ne 1 ]
  then
    echo "ERROR: nº arguments incorrecte."
    echo "Usage: $0 login"
    return $ERR_NARGS
  fi

  # 2) Validem que existeix el grup:
  gname=$1  
  gnameLine=""
  gnameLine=$(egrep "^$gname:" /etc/group 2> /dev/null)
  if [ -z "$gnameLine" ]
  then
    echo "ERROR: login $gname no existeix." >> /dev/stderr
    echo "Usage: $0 gname."
    return $ERR_NO_GNAME
  fi

  # 3) Filtrem i mostrem:
  echo "Llistat: $gname($gnameLine)"
  egrep "^[^:]*:[^:]*:[^:]*:$gnameLine:" /etc/passwd | cut -f1,3,6,7 -d:
}

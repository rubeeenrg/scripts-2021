# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Abril 2021
# Descripció: Repàs de funcions.
#

function creaEscola(){
  for classe in $*
  do
    creaClasse $classe 
  done
}


# Crear usuaris:
function creaClasse(){
  classe=$1
  PASSWD="alum"
  llista_noms=$(echo ${classe}{01..02})
  for user in $llista_noms
  do
    # Creem els usuaris i lis assignem password:
    useradd $user
    echo "$user:$PASSWD" | chpasswd   
    # echo "alum" | passwd --stdin $user &> /dev/null
  done
}

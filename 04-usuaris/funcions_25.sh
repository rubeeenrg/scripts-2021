# !/bin/bash
# @isx48062351, 2020-21, Rubén Rodríguez García
# Abril 2021
# Descripció: Processa una a una les línies del fitxer /etc/passwd i per a cada usuari mostra per stdout el size del seu home.
#

function getAllUsersSize() {
  while read -r line
  do
    getSize $(echo $line | cut -f6 -d:)
  done < /etc/passwd
}

function getSize() {
  homedir=$1
  if [ -d $homedir ]
  then
    du -s -b $homedir | cut -f1
    return 0
  else
    return 1
  fi
}
